# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.21-log)
# Database: basidati
# Generation Time: 2015-09-05 18:05:48 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table appraisals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `appraisals`;

CREATE TABLE `appraisals` (
  `player` varchar(25) NOT NULL DEFAULT '',
  `ranking` double(4,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`player`),
  CONSTRAINT `appraisals_ibfk_1` FOREIGN KEY (`player`) REFERENCES `players` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cashdesks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cashdesks`;

CREATE TABLE `cashdesks` (
  `player` varchar(25) NOT NULL DEFAULT '',
  `league` varchar(25) NOT NULL DEFAULT '',
  `payment` int(4) NOT NULL DEFAULT '0',
  `physical_examination` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`player`,`league`),
  KEY `league` (`league`),
  CONSTRAINT `cashdesk2_ibfk_3` FOREIGN KEY (`player`) REFERENCES `players` (`id`),
  CONSTRAINT `cashdesk2_ibfk_4` FOREIGN KEY (`league`) REFERENCES `leagues` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table configurations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `configurations`;

CREATE TABLE `configurations` (
  `name` varchar(100) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT 'string',
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table leagues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `leagues`;

CREATE TABLE `leagues` (
  `id` varchar(25) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `season` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table matches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matches`;

CREATE TABLE `matches` (
  `id` varchar(25) NOT NULL DEFAULT '',
  `league` varchar(25) NOT NULL DEFAULT '',
  `local` varchar(25) NOT NULL DEFAULT '',
  `guest` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `match` (`league`,`local`,`guest`),
  KEY `local` (`local`),
  KEY `guest` (`guest`),
  CONSTRAINT `matches_ibfk_1` FOREIGN KEY (`local`) REFERENCES `teams` (`id`),
  CONSTRAINT `matches_ibfk_2` FOREIGN KEY (`guest`) REFERENCES `teams` (`id`),
  CONSTRAINT `matches_ibfk_3` FOREIGN KEY (`league`) REFERENCES `leagues` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table players
# ------------------------------------------------------------

DROP TABLE IF EXISTS `players`;

CREATE TABLE `players` (
  `id` varchar(25) NOT NULL DEFAULT '',
  `team` varchar(25) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `mobile` varchar(25) NOT NULL DEFAULT '',
  `alias` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`),
  UNIQUE KEY `person` (`name`,`surname`),
  KEY `players_ibfk_1` (`team`),
  CONSTRAINT `players_ibfk_1` FOREIGN KEY (`team`) REFERENCES `teams` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`basidati`@`localhost` */ /*!50003 TRIGGER `create_ranking` AFTER INSERT ON `players` FOR EACH ROW INSERT INTO appraisals (player, ranking) VALUES (new.id, 0.0) */;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`basidati`@`localhost` */ /*!50003 TRIGGER `delete_ranking` BEFORE DELETE ON `players` FOR EACH ROW DELETE FROM appraisals WHERE player = old.id */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table playtimes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playtimes`;

CREATE TABLE `playtimes` (
  `match` varchar(25) NOT NULL DEFAULT '',
  `player` varchar(25) NOT NULL DEFAULT '',
  `minutes` int(2) NOT NULL,
  `gol` int(11) NOT NULL DEFAULT '0',
  `performance` double(4,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`match`,`player`),
  KEY `player` (`player`),
  CONSTRAINT `playtimes_ibfk_1` FOREIGN KEY (`match`) REFERENCES `matches` (`id`),
  CONSTRAINT `playtimes_ibfk_2` FOREIGN KEY (`player`) REFERENCES `players` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`basidati`@`localhost` */ /*!50003 TRIGGER `increase_ranking` AFTER INSERT ON `playtimes` FOR EACH ROW UPDATE appraisals set ranking = ranking + ((new.`minutes` * new.`performance`) / 100) + (new.`gol` / 2)
WHERE player = new.`player` */;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`basidati`@`localhost` */ /*!50003 TRIGGER `update_ranking` AFTER UPDATE ON `playtimes` FOR EACH ROW UPDATE appraisals set ranking = (ranking - (((old.`minutes` * old.`performance`) / 100) + (old.`gol` / 2))) + (((new.`minutes` * new.`performance`) / 100) + (new.`gol` / 2))
WHERE player = new.`player` */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` varchar(25) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  `division` enum('Serie A','Serie B') NOT NULL DEFAULT 'Serie A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `teamname` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
