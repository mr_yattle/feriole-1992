// folder: ${a/.png/}
// CONVERT IMAGES

for a in *.png; do cp $a ${a/.png/}/huge.png;done
for a in *.png; do sips --resampleWidth 120 $a --out ${a/.png/}/big.png;done
for a in *.png; do sips --resampleWidth 90 $a --out ${a/.png/}/medium.png;done
for a in *.png; do sips --resampleWidth 30 $a --out ${a/.png/}/small.png;done