<?php
require_once("libs/http.php");
require_once("configs/config.php");


session_start();

$user = null;
foreach($users as $item) {
    if($item["username"] == $_REQUEST['username']) {
        $user = $item;
        break;
    }
}

if(is_null($user)) {
    header("HTTP/1.0 401 Unauthorized");
    exit;
}

if($user['password'] != $_REQUEST['password']) {
    header("HTTP/1.0 401 Unauthorized");
    exit;
}

// registra utente
$_SESSION['role'] = $user["role"];

// accesso consentitno
header("HTTP/1.0 200 Accesso autorizzato");
exit;
?>