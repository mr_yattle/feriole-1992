<?php
require_once("../libs/http.php");
require_once("../libs/commons.php");
require_once("../configs/config.php");
require_once("../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized("../index.html");
}	
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../stylesheets/main.css" />
	<script type="text/javascript" src="../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        
        
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <!--<div id="logo"><img src="immagini/logo.png" title="Feriole 1992" alt="Feriole 1992" /></div>-->
		<h1>Cosa vuoi fare</h1>
<?php
if($_SESSION['role'] == 'Superuser')
{
?>
		<a href="configure/update.php" data-role="button" data-theme="b" data-icon="gear" data-ajax="false" class="ui-btn-right">Configura</a>
<?php
}
?>

	</div><!-- /header -->

	<div data-role="content">
		<ul id="main-menu" data-role="listview" data-theme="g">
<?php
if($_SESSION['role'] == 'Superuser')
{
?>
			<li>
				<a href="leagues/list.php">
					<img src="../immagini/leagues.thumb.big.png">
					<h3>Campionati</h3>
					<p>Tutte le stagioni di calcio a undici</p>
				</a>
			</li>

<?php
}
?>
			<li>
				<a href="teams/list.php">
					<img src="../immagini/teams.thumb.big.png">
					<h3>Squadre</h3>
					<p>Le squadre del campionato</p>
				</a>
			</li>

<!--			<li>
						<a href="lista/list.php">
							<img src="../immagini/membership.thumb.big.png">
							<h3>Lista CSI</h3>
							<p>Definisci i tesseramenti CSI per l'anno corrente</p>
						</a>
				</li>-->
<?php
if(can_be_used($pdo))
{
?>
			<li>
				<a href="squadra/list.php">
					<img src="../immagini/team.thumb.big.png">
					<h3>La Rosa</h3>
					<p>Gestisci la rosa della tua squadra del cuore</p>
				</a>
			</li>
			<li>
				<a href="pagamenti/list.php">
					<img src="../immagini/money.thumb.big.png">
					<h3>Cassa</h3>
					<p>Controlla la cassa e chi ha pagato quest'anno</p>
				</a>
			</li>
		    <li>
                        <a href="visita/list.php">
                                <img src="../immagini/medical.thumb.big.png">
				<h3>Visita medica</h3>
				<p>Controlla chi ha eseguito la visita medica quest'anno</p>
                        </a>
                    </li>
		    <li>
                        <a href="matches/list.php">
                                <img src="../immagini/matches.thumb.big.png">
				<h3>Incontri</h3>
				<p>Tutti gli incontri di questa stagione</p>
                        </a>
                    </li>
		    <li>
                        <a href="playtimes/list.php">
                                <img src="../immagini/playtimes.thumb.big.png">
				<h3>Minuti giocati</h3>
				<p>Registra le prestazioni dei giocatori in campo</p>
                        </a>
                    </li>
<?php
}
?>
                    <!--<li><a href="audi.html">Audi</a></li>-->
                    <!--<li><a href="bmw.html">BMW</a></li>-->
            </ul>
                                    
	</div><!-- /content -->

	<div data-position="fixed" data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>