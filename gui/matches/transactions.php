<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Insert transaction


// INSERT
if(isset($_REQUEST['insert'])) {
	$sth = $pdo->prepare('insert into matches (id, local, guest, league) values (:id, :local, :guest, :league)');
        
        // not empty check
	if(intval($_REQUEST['local']) == 1) {
		$guest = dbnormalize(stripslashes($_REQUEST['opponent']));
		$local = dbnormalize(stripslashes($_REQUEST['myteam']));
	}
	else {
		$guest = dbnormalize(stripslashes($_REQUEST['myteam']));
		$local = dbnormalize(stripslashes($_REQUEST['opponent']));
	}
        $league = dbnormalize(stripslashes($_REQUEST['league']));
        
        try {
            // start transaction
            $pdo->beginTransaction();
            $sth->execute(array(
                    ':id' => uniqid(),
                    ':guest' => $guest,
                    ':local' => $local,
                    ':league' => $league
            ));
            
            // SQLSTATE errors
            $errors = $sth->errorInfo();
            if(isset($errors[2])) {
                // OK
                header("HTTP/1.0 409 ".$errors[2]);
                exit;
            }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}

// UPDATE
if(isset($_REQUEST['update'])) {

        if(empty($_REQUEST['id'])) {
            header("HTTP/1.0 500 Internal Server Error");
            exit;
        }

	$sth = $pdo->prepare('update matches set local=:local, guest=:guest, league=:league WHERE id=:id');
        
        // not empty check
	$id = $_REQUEST['id'];
	if(intval($_REQUEST['local']) == 1) {
		$guest = dbnormalize(stripslashes($_REQUEST['opponent']));
		$local = dbnormalize(stripslashes($_REQUEST['myteam']));
	}
	else {
		$guest = dbnormalize(stripslashes($_REQUEST['myteam']));
		$local = dbnormalize(stripslashes($_REQUEST['opponent']));
	}
        $league = dbnormalize(stripslashes($_REQUEST['league']));
        
        try {
            // start transaction
            $pdo->beginTransaction();
            $sth->execute(array(
                    ':id' => $id,
                    ':guest' => $guest,
                    ':local' => $local,
                    ':league' => $league
            ));
            
            // SQLSTATE errors
            $errors = $sth->errorInfo();
            if(isset($errors[2])) {
                // OK
                header("HTTP/1.0 409 ".$errors[2]);
                exit;
            }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}

// DELETE
if(isset($_REQUEST['delete'])) {
	$sth = $pdo->prepare('delete from matches where id = :id');
        
        try {
            // start transaction
            $pdo->beginTransaction();
            $sth->execute(array(
                    ':id' => $_REQUEST['id']
            ));
            
            // SQLSTATE errors
            $errors = $sth->errorInfo();
            if(isset($errors[2])) {
                // OK
                header("HTTP/1.0 409 ".$errors[2]);
                exit;
            }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}

header("HTTP/1.0 500 Internal Server Error");
exit;
?>