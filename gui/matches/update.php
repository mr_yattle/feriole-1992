<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Retrieve record


$sql = "SELECT * FROM configurations";
$configuration = array();
foreach ($pdo->query($sql) as $row) {
    $configuration[$row['name']] = $row['value'];
}

// Retrieve avaible leagues
$sql = "SELECT * FROM leagues";
$leagues = array();
foreach ($pdo->query($sql) as $row) {
    $leagues[$row['id']] = $row['name'];
}

$sql = "SELECT * FROM teams WHERE id != '$configuration[team]'
	AND division IN (
		SELECT division FROM teams WHERE id = '$configuration[team]')";
$teams = array();
foreach ($pdo->query($sql) as $row) {
    $teams[$row['id']] = $row['name'];
}

$sql = "SELECT * FROM matches WHERE id='$_REQUEST[id]'";
foreach ($pdo->query($sql) as $row) {
    $record = $row;
}
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Modifica incontro</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/commons.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        <script type="text/javascript">
        //<![CDATA[
	$(document).ready(function() {
		$("#update").click(function() {
		    
			// Sel l'autenticazione avviene correttamente carica la pagina del menu iniziale
			$.ajax({
			    type: "POST",
					cache: false,
			    data: 'update=update&'+$("#transaction").serialize(),
			    url: $("#transaction").attr("action"),
			    success: function() {
				$.mobile.changePage("list.php");
			    },
			    error: function(request, text, http_error_msg) {
				if(request.status != null) {
				    if(request.status == 500) {
					dialog("Si è verificato un'errore nel sistema contatta l'amministratore");
				    }
				    else if(request.status == 400) {
					dialog("Hai commesso un'errore nella compilazione della maschera");
				    }
				    else if(request.status == 409) {
					dialog(http_error_msg);
				    }
				    else {
					dialog("Error Code: "+request.status+" - "+text+" HTTP("+http_error_msg+")");
				    }
				}
				
			    }
			});
		    
			return false;
		    
		});
		
		$("#delete").click(function() {
		    
		    
			// Sel l'autenticazione avviene correttamente carica la pagina del menu iniziale
			$.ajax({
					cache: false,
			    url: $("#delete").attr("href"),
			    success: function() {
						$.mobile.changePage("list.php");
			    },
			    error: function(request, text, http_error_msg) {
				if(request.status != null) {
				    if(request.status == 500) {
					dialog("Si è verificato un'errore nel sistema contatta l'amministratore");
				    }
				    else if(request.status == 400) {
					dialog("Hai commesso un'errore nella compilazione della maschera");
				    }
				    else if(request.status == 409) {
					dialog(http_error_msg);
				    }
				    else {
					dialog("Error Code: "+request.status+" - "+text+" HTTP("+http_error_msg+")");
				    }
				}
				
			    }
			});
		    
			return false;
		    
		});
	});
        //]]>
        </script>
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="list.php" data-role="button" data-icon="arrow-l">Back</a>
		<h1 class="mudule-title"><img src="../../immagini/matches.thumb.small.png" title="." alt="." /> Modifica incontro</h1>
		<a id="delete" href="transactions.php?delete=1&amp;id=<?php echo $_REQUEST['id'];?>" data-role="button" data-theme="e" data-icon="delete" data-ajax="false">Elimina</a>
		
	</div><!-- /header -->


		
	<div data-role="content">
	
                <form data-ajax="false" class="transaction" method="POST" action="transactions.php" name="transaction" id="transaction">
                
		    <input type="hidden" name="id" id="id" value="<?echo $_REQUEST['id'];?>" />
		    <input type="hidden" name="myteam" value="<? echo $configuration[team];?>" />
		
		
<?php
$on = '';
$off = 'selected="selected"';
if($record['local'] == $configuration['team']) {
	$off = '';
	$on = 'selected="selected"';
}
?>
                    <div data-role="fieldcontain">
                        <label for="local">In casa:</label>
			<select name="local" id="local" data-role="slider">
				<option value="0" <?echo $off;?>>No</option>
				<option value="1" <?echo $on;?>>Si</option>
			</select> 
                    </div>

		    <div data-role="fieldcontain">
			<label for="opponent" class="select">Avversario:</label>
			<select name="opponent" id="opponent" data-theme="a" data-icon="gear" data-inline="true" data-native-menu="false">
<?php
// Be careful guest, local
$opponent = null;
if($record['local'] == $configuration['team'])
	$opponent = $record['guest'];
else
	$opponent = $record['local'];

foreach($teams as $team_id => $team) {
	$selected = "";
	if($opponent == $team_id) {
		$selected = 'selected="selected"';
	}
	echo '
				<option value="'.$team_id.'" '.$selected.'>'.$team.'</option>';
}
?>
			</select>
		    </div>
   
		    <div data-role="fieldcontain">
			<label for="league" class="select">Stagione:</label>
			<select name="league" id="league" data-theme="a" data-icon="gear" data-inline="true" data-native-menu="false">
<?php
// Autosuggest in config if not selected by user
$current_league = $configuration[league];
if(isset($record[league])) {
	$current_league = $record[league];
}

foreach($leagues as $league_id => $league) {
	$selected = "";
	if($league_id == $current_league) {
		$selected = 'selected="selected"';
	}
	echo '
				<option value="'.$league_id.'" '.$selected.'>'.$league.'</option>';
}
?>
			</select>
		    </div>
   
                
                    <div class="actions" data-role="fieldcontain">
                        <input data-icon="plus" data-theme="b" type="submit" name="update" id="update" value="Salva" />
                    </div>
                
                </form>
                                    
	</div><!-- /content -->

	<div data-position="fixed"  data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>