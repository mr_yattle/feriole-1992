<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}	
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Quote versate</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        
        
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="../menu.php" data-role="button" data-icon="arrow-l">Back</a>
		<h1 class="mudule-title"><img src="../../immagini/money.thumb.small.png" title="." alt="." />&nbsp;&nbsp; Cassa</h1>
		
		<div data-role="navbar">
			<ul>
				<li><a data-icon="grid" href="list.php" <?if(!isset($_REQUEST['pagato'])) echo 'class="ui-btn-active"';?>>Tutti</a></li>
				<li><a data-icon="alert" href="list.php?pagato=0" <?if(isset($_REQUEST['pagato']) && ($_REQUEST['pagato'] == 0)) echo 'class="ui-btn-active"';?>>Non hanno pagato</a></li> <!-- -->
				<li><a data-icon="star" href="list.php?pagato=1" <?if(isset($_REQUEST['pagato']) && ($_REQUEST['pagato'] == 1)) echo 'class="ui-btn-active"';?>>Hanno pagato</a></li>
			</ul>
		</div><!-- /navbar -->
	</div><!-- /header -->


		
	<div data-role="content">
<?php	
// Insert transaction


// Configurazione
$sql = "SELECT * FROM configurations";
$configuration = array();
foreach ($pdo->query($sql) as $row) {
    $configuration[$row['name']] = $row['value'];
}

$data = array();
$sql = "SELECT players.id, players.name, players.surname, cashdesks.payment
	FROM players
	LEFT JOIN cashdesks
		ON cashdesks.player = players.id
		AND cashdesks.league = '$configuration[league]'
	WHERE players.team = '$configuration[team]'";

// Hanno effettuato il pagamento	
if(isset($_REQUEST['pagato']) && ($_REQUEST['pagato'] == 0)) {
	$sql .= "
	AND (cashdesks.payment = 0 OR cashdesks.payment IS NULL)";
}
else if(isset($_REQUEST['pagato']) && ($_REQUEST['pagato'] == 1)) {
	$sql .= "
	AND cashdesks.payment > 0";
}

$sql .= "
	ORDER BY players.surname";

foreach ($pdo->query($sql) as $row) {
    $letter = strtoupper(substr($row['surname'], 0, 1));
    $data[$letter][] = $row;
}
?>
	
		<div class="content-primary">	
			<ul data-role="listview" data-filter="true" data-filter-placeholder="Cerca giocatori..." data-filter-theme="b">
<?php
if(sizeof($data) == 0) {

	echo '
		<li data-role="list-divider">Nessun pagamento registrato in cassa</li>';

}
?>			
				<!--<li><a href="index.html">Adam Kinkaid</a></li>-->
				<!--<li><a href="index.html">Alex Wickerham</a></li>-->
				<!--<li><a href="index.html">Avery Johnson</a></li>-->
				<!--<li data-role="list-divider">B</li>-->
<?php
foreach($data as $letter => $players) {
	echo '
				<li data-role="list-divider">'.$letter.'</li>';

	foreach($players as $player) {
		if(is_null($player['payment']) || $player['payment'] == 0) {
			echo '
			<li data-role="fieldcontain" data-icon="alert">
				<a href="update.php?player='.$player['id'].'&amp;league='.$configuration['league'].'" data-ajax="false">
					<strong>'.$player['surname'].' '.$player['name'].'</strong>
				</a>
			</li>';
		}
		else {
			// Good players pay all their dues
			$icon = "";
			if($player['payment'] == $configuration['quota'])
				$icon = 'data-icon="star"';
				
			echo '
			<li data-role="fieldcontain" $icon>
				<a href="update.php?player='.$player['id'].'&amp;league='.$configuration['league'].'" data-ajax="false">
					<strong>'.$player['surname'].' '.$player['name'].'</strong>
					<span class="ui-li-count">'.$player['payment'].'</span>
				</a>
			</li>';
		}

	}				
}
?>
			</ul>
		</div><!--/content-primary -->	
                                    
	</div><!-- /content -->

	<!-- /footer <div data-position="fixed" data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div>-->
</div><!-- /page -->

</body>
</html>