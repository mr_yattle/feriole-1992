<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Retrieve record


$sql = "SELECT * FROM configurations";
$configuration = array();
foreach ($pdo->query($sql) as $row) {
    $configuration[$row['name']] = $row['value'];
}

// Retrieve player settings
$sql = "SELECT * FROM players WHERE id = '$_REQUEST[player]'";
$player_name = "";
foreach ($pdo->query($sql) as $row) {
    $player_name = "$row[name] $row[surname]";
}

// Retrieve avaible leagues
$sql = "SELECT * FROM leagues";
$leagues = array();
foreach ($pdo->query($sql) as $row) {
    $leagues[$row['id']] = $row['name'];
}

// Retrieve cashdesk settings
$sql = "SELECT * FROM cashdesks WHERE player = '$_REQUEST[player]' AND  league = '$_REQUEST[league]'";
$record = array();
foreach ($pdo->query($sql) as $row) {
    $record = $row;
}
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Registra versamento</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/commons.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        <script type="text/javascript">
        //<![CDATA[
	$(document).ready(function() {
		$("#update").click(function() {
		    
			// Sel l'autenticazione avviene correttamente carica la pagina del menu iniziale
			$.ajax({
			    type: "POST",
					cache: false,
			    data: 'update=update&'+$("#transaction").serialize(),
			    url: $("#transaction").attr("action"),
			    success: function() {
				$.mobile.changePage("list.php");
			    },
			    error: function(request, text, http_error_msg) {
				if(request.status != null) {
				    if(request.status == 500) {
					dialog("Si è verificato un'errore nel sistema contatta l'amministratore");
				    }
				    else if(request.status == 400) {
					dialog("Hai commesso un'errore nella compilazione della maschera");
				    }
				    else if(request.status == 409) {
					dialog(http_error_msg);
				    }
				    else {
					dialog("Error Code: "+request.status+" - "+text+" HTTP("+http_error_msg+")");
				    }
				}
				
			    }
			});
		    
			return false;
		    
		});
	});
        //]]>
        </script>
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="list.php" data-role="button" data-icon="arrow-l">Back</a>
		<h1>Registra versamento</h1>
		
	</div><!-- /header -->


		
	<div data-role="content">
	
                <form data-ajax="false" class="transaction" method="POST" action="transactions.php" name="transaction" id="transaction">
		
		    <div data-role="fieldcontain">
                        <label for="player_name">Giocatore:</label>
			<input type="text" name="player_name" id="player_name" maxlength="25" readonly="readonly" value="<?php echo $player_name;?>" />
			<input type="hidden" name="player" id="player" value="<?echo $_REQUEST['player'];?>" />
                    </div>
		
		
                    <div data-role="fieldcontain">
                        <label for="payment">Quota versata:</label>
                        <input min="0" max="<?php echo $configuration[quota];?>" data-type="range" type="number" name="payment" id="payment" maxlength="8" value="<?php echo empty($record['payment']) ? 0 : $record['payment'];?>" />
                    </div>

		    
		    <div data-role="fieldcontain">
			<label for="league" class="select">Stagione:</label>
			<select name="league" id="league" data-theme="a" data-icon="gear" data-inline="true" data-native-menu="false">
<?php
// Autosuggest in config if not selected by user
$current_league = $configuration[league];
if(isset($record[league])) {
	$current_league = $record[league];
}

foreach($leagues as $league_id => $league) {
	$selected = "";
	if($league_id == $current_league) {
		$selected = 'selected="selected"';
	}
	echo '
				<option value="'.$league_id.'" '.$selected.'>'.$league.'</option>';
}
?>
			</select>
		    </div>
		    
                
                    <div class="actions" data-role="fieldcontain">
                        <input data-icon="refresh" data-theme="b" type="submit" name="update" id="update" value="Salva" />
                    </div>
                
                </form>
                                    
	</div><!-- /content -->

	<div data-position="fixed" data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>