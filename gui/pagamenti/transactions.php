<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Insert transaction



// UPDATE
if(isset($_REQUEST['update'])) {

        if(empty($_REQUEST['player'])) {
            header("HTTP/1.0 500 Internal Server Error");
            exit;
        }
	
        // not empty check
        $player = $_REQUEST['player'];
        $payment = dbnormalize(stripslashes($_REQUEST['payment']));
        $league = dbnormalize(stripslashes($_REQUEST['league']));
        
        if(empty($payment)) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }
        
        if(empty($league)) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }
        
        try {
            // start transaction
            $pdo->beginTransaction();
	    
		$sql = "SELECT count(*) as tot FROM cashdesks WHERE player = '$player' AND  league = '$league'";
		$count = array();
		foreach($pdo->query($sql) as $row) {
			$count = $row;
		}
		// Check before insert if value exists
		if($row['tot'] == 0) {
			$sth = $pdo->prepare('insert into cashdesks (player, payment, league, physical_examination) values(:player, :payment, :league, :physical_examination)');
			$sth->execute(array(
				':player' => $player,
				':payment' => $payment,
				':league' => $league,
				':physical_examination' => 0
			));
		}
		else {
			$sth = $pdo->prepare('update cashdesks set payment=:payment where player=:player and league=:league');
			$sth->execute(array(
				':player' => $player,
				':payment' => $payment,
				':league' => $league
			));
		}
	    
            
            // SQLSTATE errors
            $errors = $sth->errorInfo();
            if(isset($errors[2])) {
                // OK
                header("HTTP/1.0 409 ".$errors[2]);
                exit;
            }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}

header("HTTP/1.0 500 Internal Server Error");
exit;
?>