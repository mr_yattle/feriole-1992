<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Retrieve record


$sql = "SELECT * FROM leagues WHERE id='$_REQUEST[id]'";
foreach ($pdo->query($sql) as $row) {
    $record = $row;
}
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Modifica stagione</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/commons.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        <script type="text/javascript">
        //<![CDATA[
	$(document).ready(function() {
		$("#update").click(function() {
		    
			// Controlli
			if(isEmpty($("#name").val())) {
			    dialog("Il nome della stagione è obbligatorio");
			    return false;
			}
		    
		    
			// Sel l'autenticazione avviene correttamente carica la pagina del menu iniziale
			$.ajax({
			    type: "POST",
					cache: false,
			    data: 'update=update&'+$("#transaction").serialize(),
			    url: $("#transaction").attr("action"),
			    success: function() {
				$.mobile.changePage("list.php");
			    },
			    error: function(request, text, http_error_msg) {
				if(request.status != null) {
				    if(request.status == 500) {
					dialog("Si è verificato un'errore nel sistema contatta l'amministratore");
				    }
				    else if(request.status == 400) {
					dialog("Hai commesso un'errore nella compilazione della maschera");
				    }
				    else if(request.status == 409) {
					dialog(http_error_msg);
				    }
				    else {
					dialog("Error Code: "+request.status+" - "+text+" HTTP("+http_error_msg+")");
				    }
				}
				
			    }
			});
		    
			return false;
		    
		});
		
		$("#delete").click(function() {
		    
		    
			// Sel l'autenticazione avviene correttamente carica la pagina del menu iniziale
			$.ajax({
					cache: false,
			    url: $("#delete").attr("href"),
			    success: function() {
						$.mobile.changePage("list.php");
			    },
			    error: function(request, text, http_error_msg) {
				if(request.status != null) {
				    if(request.status == 500) {
					dialog("Si è verificato un'errore nel sistema contatta l'amministratore");
				    }
				    else if(request.status == 400) {
					dialog("Hai commesso un'errore nella compilazione della maschera");
				    }
				    else if(request.status == 409) {
					dialog(http_error_msg);
				    }
				    else {
					dialog("Error Code: "+request.status+" - "+text+" HTTP("+http_error_msg+")");
				    }
				}
				
			    }
			});
		    
			return false;
		    
		});
	});
        //]]>
        </script>
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="list.php" data-role="button" data-icon="arrow-l">Back</a>
		<h1 class="mudule-title"><img src="../../immagini/leagues.thumb.small.png" title="." alt="." /> Modifica stagione</h1>
		<a id="delete" href="transactions.php?delete=16&amp;id=<?php echo $_REQUEST['id'];?>" data-role="button" data-theme="e" data-icon="delete" data-ajax="false">Elimina</a>
		
	</div><!-- /header -->


		
	<div data-role="content">
	
                <form data-ajax="false" class="transaction" method="POST" action="transactions.php" name="transaction" id="transaction">
                
		    <input type="hidden" name="id" id="id" value="<?php echo $_REQUEST['id'];?>" />
		
                    <div data-role="fieldcontain">
                        <label for="name">Nome:</label>
                        <input type="text" name="name" id="name" maxlength="25" value="<?php echo $record['name'];?>" />
                    </div>
                
                    <div class="actions" data-role="fieldcontain">
                        <input data-icon="refresh" data-theme="b" type="submit" name="update" id="update" value="Salva" />
                    </div>
                
                </form>
                                    
	</div><!-- /content -->

	<div data-position="fixed" data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>