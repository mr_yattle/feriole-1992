<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Insert transaction


// INSERT
if(isset($_REQUEST['insert'])) {
	$sth = $pdo->prepare('insert into leagues (id, name) values (:id, :name)');
        
        // not empty check
        $name = dbnormalize(stripslashes($_REQUEST['name']));
        
        if(empty($name)) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }
        
        try {
            // start transaction
            $pdo->beginTransaction();
            $sth->execute(array(
                    ':id' => uniqid(),
                    ':name' => $name
            ));
            
            // SQLSTATE errors
            $errors = $sth->errorInfo();
            if(isset($errors[2])) {
                // OK
                header("HTTP/1.0 409 ".$errors[2]);
                exit;
            }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}

// UPDATE
if(isset($_REQUEST['update'])) {

        if(empty($_REQUEST['id'])) {
            header("HTTP/1.0 500 Internal Server Error");
            exit;
        }

	$sth = $pdo->prepare('update leagues set name=:name WHERE id=:id');
        
        // not empty check
        $id = $_REQUEST['id'];
        $name = dbnormalize(stripslashes($_REQUEST['name']));
        
        if(empty($name)) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }

        try {
            // start transaction
            $pdo->beginTransaction();
            $sth->execute(array(
                    ':id' => $id,
                    ':name' => $name
            ));
            
            // SQLSTATE errors
            $errors = $sth->errorInfo();
            if(isset($errors[2])) {
                // OK
                header("HTTP/1.0 409 ".$errors[2]);
                exit;
            }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}


// DELETE
if(isset($_REQUEST['delete'])) {
	$sth = $pdo->prepare('delete from leagues where id = :id');
        
        try {
            // start transaction
            $pdo->beginTransaction();
            $sth->execute(array(
                    ':id' => $_REQUEST['id']
            ));
            
            // SQLSTATE errors
            $errors = $sth->errorInfo();
            if(isset($errors[2])) {
                // OK
                header("HTTP/1.0 409 ".$errors[2]);
                exit;
            }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}


header("HTTP/1.0 500 Internal Server Error");
exit;
?>