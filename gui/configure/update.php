<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Retrieve record


$sql = "SELECT * FROM teams";
$teams = array();
foreach ($pdo->query($sql) as $row) {
    $teams[$row['id']] = $row['name'];
}

$sql = "SELECT * FROM leagues";
$leagues = array();
foreach ($pdo->query($sql) as $row) {
    $leagues[$row['id']] = $row['name'];
}

$sql = "SELECT * FROM configurations";
$record = array();
foreach ($pdo->query($sql) as $row) {
    $record[$row['name']] = $row['value'];
}

?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Modifica giocatore</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/commons.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        <script type="text/javascript">
        //<![CDATA[
	$(document).ready(function() {
		$("#update").click(function() {
		    
			// Controlli
			if(isEmpty($("#league").val())) {
			    dialog("Devi specificare la stagione (ricorda di definire le stagioni nella sezione Campinati)");
			    return false;
			}
			if(isEmpty($("#team").val())) {
			    dialog("Devi specificare la tua squadra (ricorda di definire le squadre nella sezione Squadre)");
			    return false;
			}
		    
			// Sel l'autenticazione avviene correttamente carica la pagina del menu iniziale
			$.ajax({
			    type: "POST",
					cache: false,
			    data: 'update=update&'+$("#transaction").serialize(),
			    url: $("#transaction").attr("action"),
			    success: function() {
				dialog("Configurazione salvata con successo");
			    },
			    error: function(request, text, http_error_msg) {
				if(request.status != null) {
				    if(request.status == 500) {
					dialog("Si è verificato un'errore nel sistema contatta l'amministratore");
				    }
				    else if(request.status == 400) {
					dialog("Hai commesso un'errore nella compilazione della maschera");
				    }
				    else if(request.status == 409) {
					dialog(http_error_msg);
				    }
				    else {
					dialog("Error Code: "+request.status+" - "+text+" HTTP("+http_error_msg+")");
				    }
				}
				
			    }
			});
		    
			return false;
		    
		});
	});
        //]]>
        </script>
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="../menu.php" data-role="button" data-icon="arrow-l">Back</a>
		<h1>Configurazioni</h1>
		
	</div><!-- /header -->


		
	<div data-role="content">
	
                <form data-ajax="false" class="transaction" method="POST" action="transactions.php" name="transaction" id="transaction">
		
                    <div data-role="fieldcontain">
                        <label for="quota">Quota annuale:</label>
                        <input min="0" max="300" data-type="range" type="number" name="configuration[quota]" id="quota" maxlength="25" value="<?php echo $record[quota];?>" />
                    </div>
		    
		    <div data-role="fieldcontain">
			<label for="league" class="select">Stagione:</label>
			<select name="configuration[league]" id="league" data-theme="a" data-icon="gear" data-inline="true" data-native-menu="false">
<?php
foreach($leagues as $league_id => $league) {
	$selected = "";
	if($league_id == $record[league]) {
		$selected = 'selected="selected"';
	}
	echo '
				<option value="'.$league_id.'" '.$selected.'>'.$league.'</option>';
}
?>
			</select>
		    </div>
		    
                    <div data-role="fieldcontain">
                        <label for="quota">Durata di un' incontro:</label>
                        <input min="0" max="90" data-type="range" type="number" name="configuration[duration]" id="duration" maxlength="25" value="<?php echo $record[duration];?>" />
                    </div>
		    
		    <div data-role="fieldcontain">
			<label for="team" class="select">Il nostro team:</label>
			<select name="configuration[team]" id="team" data-theme="a" data-icon="gear" data-inline="true" data-native-menu="false">
<?php
foreach($teams as $team_id => $team) {
	$selected = "";
	if($record[team] == $team_id) {
		$selected = 'selected="selected"';
	}
	echo '
				<option value="'.$team_id.'" '.$selected.'>'.$team.'</option>';
}
?>
			</select>
		    </div>
                
                    <div class="actions" data-role="fieldcontain">
                        <input data-icon="refresh" data-theme="b" type="submit" name="update" id="update" value="Salva" />
                    </div>
                
                </form>
                                    
	</div><!-- /content -->

	<div data-position="fixed" data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>