<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Insert transaction


// UPDATE
if(isset($_REQUEST['update'])) {

        if(empty($_REQUEST['configuration'])) {
            header("HTTP/1.0 500 Internal Server Error");
            exit;
        }

	
        

        
        try {
            // start transaction
            $pdo->beginTransaction();
	    
	    // set all configurations
	    foreach($_REQUEST['configuration'] as $name => $value) {
	    
		// not empty check
		$value = dbnormalize(stripslashes($value));
		
		if(empty($value)) {
		    header("HTTP/1.0 400 Bad Request");
		    exit;
		}
		

		$sql = "SELECT count(*) as tot FROM configurations WHERE name = '$name'";
		$count = array();
		foreach($pdo->query($sql) as $row) {
			$count = $row;
		}
		
		// Check before insert if value exists
		if($row['tot'] == 0) {
			$sth = $pdo->prepare('insert into configurations (name, type, value) values(:name, \'string\', :value)');
			$sth->execute(array(
			    ':name' => $name,
			    ':value' => $value
			));
		}
		else {
			$sth = $pdo->prepare('update configurations set value=:value where name=:name');
			$sth->execute(array(
			    ':value' => $value,
			    ':name' => $name
			));
		}

		// kay name duplication
		if(isset($errors[2])) {
		    // OK
		    header("HTTP/1.0 409 ".$errors[2]);
		    exit;
		}
		
		
	    }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}

header("HTTP/1.0 500 Internal Server Error");
exit;
?>