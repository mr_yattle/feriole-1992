<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Insert transaction



// UPDATE
if(isset($_REQUEST['update'])) {

        if(empty($_REQUEST['id'])) {
            header("HTTP/1.0 500 Internal Server Error");
            exit;
        }
	
        // not empty check
        $id = $_REQUEST['id'];
        $iscritto = dbnormalize(stripslashes($_REQUEST['iscritto']));
        $league = dbnormalize(stripslashes($_REQUEST['league']));
        
	
        if(!isset($_REQUEST['iscritto'])) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }
        
        if(empty($league)) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }

        try {
            // start transaction
            $pdo->beginTransaction();
	    
		$sql = "SELECT count(*) as tot FROM membership WHERE id = '$id' and league = '$league'";
		$count = array();
		foreach($pdo->query($sql) as $row) {
			$count = $row;
		}
	    
		// Check before insert if value exists
		if($row['tot'] == 0 && $iscritto == 1) {
			$sth = $pdo->prepare('insert into membership (id, league) values(:id, :league)');
			$sth->execute(array(
				':id' => $id,
				':league' => $league
			));
		}
		if($iscritto == 0) {
			$sth = $pdo->prepare('delete from membership where id=:id and league=:league');
			$sth->execute(array(
				':id' => $id,
				':league' => $league
			));
		}
	    
	    
            
            // SQLSTATE errors
            $errors = $sth->errorInfo();
            if(isset($errors[2])) {
                // OK
                header("HTTP/1.0 409 ".$errors[2]);
                exit;
            }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}
if(isset($_REQUEST['insert'])) {
				
        // not empty check
        $league = dbnormalize(stripslashes($_REQUEST['league']));
        
        if(empty($league)) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }
				
        try {
            // start transaction
            $pdo->beginTransaction();
						
				$sth = $pdo->prepare('insert into membership (id, league) select id, :league from players');
				$sth->execute(array(
					':league' => $league
				));
						
						
						
            // end transaction
            $pdo->commit();
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
            // OK
				    header('Location: list.php');
            exit;

				
}

header("HTTP/1.0 500 Internal Server Error");
exit;
?>