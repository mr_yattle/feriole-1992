<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}


// Configurazione
$sql = "SELECT * FROM configurations";
$configuration = array();
foreach ($pdo->query($sql) as $row) {
    $configuration[$row['name']] = $row['value'];
}

// Aggiunti tutti ?
$sql = "SELECT count(*) as tot FROM membership WHERE league = '".$configuration[league]."'";
$count = array();
foreach($pdo->query($sql) as $row) {
	$count = $row;
}
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Lista tesseramenti</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="../menu.php" data-role="button" data-icon="arrow-l">Back</a>
		<h1 class="mudule-title" id="transaction"><img src="../../immagini/membership.thumb.small.png" title="." alt="." />&nbsp;&nbsp; Lista tessermenti</h1>
		<?php
			if($count[tot] == 0)
				echo '
		<a  href="transactions.php?insert=insert&amp;league='.$configuration[league].'" data-role="button" data-theme="b" data-icon="plus" data-ajax="true">Tessera tutti</a>';
		?>
		
		<div data-role="navbar">
			<ul>
				<li><a data-icon="grid" href="list.php" <?if(!isset($_REQUEST['iscritto'])) echo 'class="ui-btn-active"';?>>Rosa</a></li>
				<li><a data-icon="alert" href="list.php?iscritto=0" <?if(isset($_REQUEST['iscritto']) && ($_REQUEST['iscritto'] == 0)) echo 'class="ui-btn-active"';?>>Non iscritti</a></li> <!-- -->
				<li><a data-icon="star" href="list.php?iscritto=1" <?if(isset($_REQUEST['iscritto']) && ($_REQUEST['iscritto'] == 1)) echo 'class="ui-btn-active"';?>>Iscritti</a></li>
			</ul>
		</div><!-- /navbar -->
	</div><!-- /header -->


		
	<div data-role="content">
<?php	
$data = array();
$sql = "SELECT players.id, players.name, players.surname, membership.id as iscritto
	FROM players
	LEFT JOIN membership
		ON membership.id = players.id
		AND membership.league = '$configuration[league]'
	WHERE players.deleted = 0";

// Hanno effettuato il pagamento	
if(isset($_REQUEST['iscritto']) && ($_REQUEST['iscritto'] == 0)) {
	$sql .= "
	AND membership.id IS NULL";
}
else if(isset($_REQUEST['iscritto']) && ($_REQUEST['iscritto'] == 1)) {
	$sql .= "
	AND membership.id IS NOT NULL";
}

$sql .= "
	ORDER BY players.surname";
	
foreach ($pdo->query($sql) as $row) {
    $letter = strtoupper(substr($row['surname'], 0, 1));
    $data[$letter][] = $row;
}
?>
	
		<div class="content-primary">	
			<ul data-role="listview" data-filter="true" data-filter-placeholder="Cerca giocatori..." data-filter-theme="b">
<?php
if(sizeof($data) == 0) {
	if($_REQUEST['iscritto'] == 1)
		echo '
			<li data-role="list-divider">Nessun giocatore iscritto</li>';
	else
		echo '
			<li data-role="list-divider">Tutti i giocatori sono stati iscritti</li>';

}
?>			
				<!--<li><a href="index.html">Adam Kinkaid</a></li>-->
				<!--<li><a href="index.html">Alex Wickerham</a></li>-->
				<!--<li><a href="index.html">Avery Johnson</a></li>-->
				<!--<li data-role="list-divider">B</li>-->
<?php
foreach($data as $letter => $players) {
	echo '
				<li data-role="list-divider">'.$letter.'</li>';

	foreach($players as $player) {
		if(is_null($player['iscritto']) || $player['iscritto'] == 0) {
			echo '
			<li data-role="fieldcontain" data-icon="alert">
				<a href="update.php?id='.$player['id'].'&amp;league='.$configuration['league'].'" data-ajax="false">
					<strong>'.$player['surname'].' '.$player['name'].'</strong>
				</a>
			</li>';
		}
		else {
			// Good players pay all their dues
			echo '
			<li data-role="fieldcontain" data-icon="star">
				<a href="update.php?id='.$player['id'].'&amp;league='.$configuration['league'].'" data-ajax="false">
					<strong>'.$player['surname'].' '.$player['name'].'</strong>
				</a>
			</li>';
		}

	}				
}
?>
			</ul>
		</div><!--/content-primary -->	
                                    
	</div><!-- /content -->

	<!-- /footer <div data-position="fixed" data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div>-->
</div><!-- /page -->

</body>
</html>