<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}	
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Minuti giocati dalla rosa</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        
        
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="../menu.php" data-role="button" data-icon="arrow-l">Back</a>
		<h1 class="mudule-title"><img src="../../immagini/playtimes.thumb.small.png" title="." alt="." />&nbsp;&nbsp;Minuti giocati</h1>
		
	</div><!-- /header -->


		
	<div data-role="content">
<?php	
// Insert transaction


// Configurazione
$sql = "SELECT * FROM configurations";
$configuration = array();
foreach ($pdo->query($sql) as $row) {
    $configuration[$row['name']] = $row['value'];
}


$data = array();
$sql = "SELECT matches.id, locals.id AS lid, locals.name AS local, guests.id AS gid, guests.name AS guest
	FROM matches
	LEFT JOIN teams AS locals ON locals.id = matches.local
	LEFT JOIN teams AS guests ON guests.id = matches.guest
	WHERE matches.league = '$configuration[league]'
	AND (locals.id = '$configuration[team]' OR guests.id = '$configuration[team]')";
foreach ($pdo->query($sql) as $row) {
    $data[$row[id]] = $row;
}
?>
	
		<div class="content-primary">
			<ul data-role="listview">			

<?php
foreach($data as $id => $team) {

	$opponent = null;
	if($configuration[team] == $team[lid]) {
		$opponent = $team['guest'];
	}
	else {
		$opponent = $team['local'];
	}

	echo '
	<li>
		<a href="populate.php?match='.$id.'">
			<h3>'.$team['local'].' - '.$team['guest'].'</h3>
			<p>Definisci quali giocatori sono scesi in campo e per quanto tempo</p>
		</a>
	</li>';		
}
?>
			</ul>
		</div><!--/content-primary -->	
                                    
	</div><!-- /content -->

	<!-- /footer <div data-position="fixed" data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div>-->
</div><!-- /page -->

</body>
</html>