<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Retrieve avaible leagues
$sql = "SELECT * FROM leagues";
$leagues = array();
foreach ($pdo->query($sql) as $row) {
    $leagues[$row['id']] = $row['name'];
}

$sql = "SELECT * FROM configurations";
$configuration = array();
foreach ($pdo->query($sql) as $row) {
    $configuration[$row['name']] = $row['value'];
}

// Retrieve player settings
$sql = "SELECT * FROM players WHERE id = '$_REQUEST[player]'";
$player = "";
foreach ($pdo->query($sql) as $row) {
    $player = "$row[name] $row[surname]";
}

// Retrieve match settings
$sql = "SELECT matches.id, locals.name AS local, guests.name AS guest
	FROM matches
	LEFT JOIN teams AS locals ON locals.id = matches.local
	LEFT JOIN teams AS guests ON guests.id = matches.guest
	WHERE matches.id = '$_REQUEST[match]'
	AND matches.league = '$configuration[league]'
	AND (locals.id = '$configuration[team]' OR guests.id = '$configuration[team]')";
$match = "";
foreach ($pdo->query($sql) as $row) {
    $match = "$row[local] - $row[guest] ";
}

// Retrieve cashdesk settings
$sql = "SELECT * FROM playtimes WHERE player = '$_REQUEST[player]' AND `match` = '$_REQUEST[match]'";
$record = array();
foreach ($pdo->query($sql) as $row) {
    $record = $row;
}

// decido se rifiltrare la pagina
$giocato_url = '';
	
// In campo	
if(isset($_REQUEST['giocato']) && ($_REQUEST['giocato'] == 0)) {
	$giocato_url = "&giocato=0";
}
else if(isset($_REQUEST['giocato']) && ($_REQUEST['giocato'] == 1)) {
	$giocato_url = "&giocato=1";
}
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Scenti in campo</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/commons.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        <script type="text/javascript">
        //<![CDATA[
	$(document).ready(function() {
		$("#update").click(function() {
		    
			// Sel l'autenticazione avviene correttamente carica la pagina del menu iniziale
			$.ajax({
			    type: "POST",
					cache: false,
			    data: 'update=update&'+$("#transaction").serialize(),
			    url: $("#transaction").attr("action"),
			    success: function() {
				$.mobile.changePage("populate.php?player=<?echo $_REQUEST[player];?>&match=<?echo $_REQUEST[match];?><?echo $giocato_url;?>");
			    },
			    error: function(request, text, http_error_msg) {
				if(request.status != null) {
				    if(request.status == 500) {
					dialog("Si è verificato un'errore nel sistema contatta l'amministratore");
				    }
				    else if(request.status == 400) {
					dialog("Hai commesso un'errore nella compilazione della maschera");
				    }
				    else if(request.status == 409) {
					dialog(http_error_msg);
				    }
				    else {
					dialog("Error Code: "+request.status+" - "+text+" HTTP("+http_error_msg+")");
				    }
				}
				
			    }
			});
		    
			return false;
		    
		});
	});
        //]]>
        </script>
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="populate.php?match=<? echo $_REQUEST[match].$giocato_url; ?>" data-role="button" data-icon="arrow-l">Back</a>
		<h1>In campo</h1>
		
	</div><!-- /header -->


		
	<div data-role="content">
	
                <form data-ajax="false" class="transaction" method="POST" action="transactions.php" name="transaction" id="transaction">
		
		    <div data-role="fieldcontain">
                        <label for="player_description">Giocatore:</label>
			<input type="text" name="player_description" id="player_description" maxlength="25" readonly="readonly" value="<?php echo $player;?>" />
			<input type="hidden" name="player" id="player" value="<?echo $_REQUEST['player'];?>" />
                    </div>
		    
		    <div data-role="fieldcontain">
                        <label for="match_descritpion">Incontro:</label>
			<input type="text" name="match_descritpion" id="match_descritpion" maxlength="25" readonly="readonly" value="<?php echo $match;?>" />
			<input type="hidden" name="match" id="match" value="<?echo $_REQUEST['match'];?>" />
                    </div>
		
<?php
$minutes = $configuration[duration];
if(isset($record[minutes])) {
	$minutes = $record[minutes];
}
?>
                    <div data-role="fieldcontain">
                        <label for="minutes">Minuti giocati:</label>
                        <input min="0" max="<?php echo $configuration[duration];?>" data-type="range" type="number" name="minutes" id="minutes" maxlength="8" value="<?php echo $minutes;?>" />
                    </div>
										
<?php
$gol = 0;
if(isset($record[gol])) {
	$gol = $record[gol];
}
?>
                    <div data-role="fieldcontain">
                        <label for="minutes">Gol fatti:</label>
                        <input min="0" max="10" data-type="range" type="number" name="gol" id="gol" maxlength="8" value="<?php echo $gol;?>" />
                    </div>
										
										<label for="performance" class="select">Prestazione:</label>
												<select name="performance" id="performance" data-theme="a" data-icon="gear" data-inline="true" data-native-menu="false">
<?php
$performances = array(
		array("value" => 0.5, "svalue" => '0.5', "label" => "Mediocre"),
		array("value" => 1.0, "svalue" => '1.0', "label" => "Buono"),
		array("value" => 1.5, "svalue" => '1.5', "label" => "Ottimo"));

// Autosuggest in config if not selected by user
$current_league = $configuration[league];
if(isset($record[league])) {
	$current_league = $record[league];
}

$default_performance = 1.0;
if(isset($record[performance])) {
		$default_performance = $record[performance];
}

foreach($performances as $performance) {
	$selected = "";
	if($performance["value"] == $default_performance) {
		$selected = 'selected="selected"';
	}
	echo '
														<option value="'.$performance["svalue"].'" '.$selected.'>'.$performance["label"].'</option>';
}
?>
												</select>
										</div>
                
                    <div class="actions" data-role="fieldcontain">
                        <input data-icon="refresh" data-theme="b" type="submit" name="update" id="update" value="Salva" />
                    </div>
                
                </form>
                                    
	</div><!-- /content -->

	<div data-position="fixed" data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>