<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Insert transaction



// UPDATE
if(isset($_REQUEST['update'])) {

        if(empty($_REQUEST['player'])) {
            header("HTTP/1.0 500 Internal Server Error");
            exit;
        }
	
        if(empty($_REQUEST['match'])) {
            header("HTTP/1.0 500 Internal Server Error");
            exit;
        }
	
        // not empty check
        $player = $_REQUEST['player'];
				$match = $_REQUEST['match'];
        $minutes = dbnormalize(stripslashes($_REQUEST['minutes']));
        $gol = dbnormalize(stripslashes($_REQUEST['gol']));
        $performance = dbnormalize(stripslashes($_REQUEST['performance']));
        
        if(empty($minutes) && $minutes != 0) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }

        try {
            // start transaction
            $pdo->beginTransaction();
	    
		$sql = "SELECT count(*) as tot FROM playtimes WHERE player = '$player' AND `match` = '$match'";
		$count = array();
		foreach($pdo->query($sql) as $row) {
			$count = $row;
		}
	    
		// Check before insert if value exists
		if($row['tot'] == 0) {
			$sth = $pdo->prepare('insert into playtimes (minutes, player, `match`, gol, performance) values(:minutes, :player, :match, :gol, :performance)');
			$sth->execute(array(
				':minutes' => $minutes,
				':match' => $match,
				':player' => $player,
				':gol' => $gol,
				':performance' => $performance
			));
		}
		else {
			$sth = $pdo->prepare('update playtimes set minutes=:minutes, gol=:gol, performance=:performance  where player=:player AND `match`=:match');
			$sth->execute(array(
				':minutes' => $minutes,
				':match' => $match,
				':player' => $player,
				':gol' => $gol,
				':performance' => $performance
			));
		}
	    
	    
            
            // SQLSTATE errors
            $errors = $sth->errorInfo();
            if(isset($errors[2])) {
                // OK
                header("HTTP/1.0 409 ".$errors[2]);
                exit;
            }
            
            // end transaction
            $pdo->commit();
            
            // OK
            header("HTTP/1.0 200 OK");
            exit;
            
        }
        catch(Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
                exit;
        }
}

header("HTTP/1.0 500 Internal Server Error");
exit;
?>