<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}

// Insert transaction


// Configurazione
$sql = "SELECT * FROM configurations";
$configuration = array();
foreach ($pdo->query($sql) as $row) {
    $configuration[$row['name']] = $row['value'];
}

// Retrieve match settings
$sql = "SELECT matches.id, locals.name AS local, guests.name AS guest
	FROM matches
	LEFT JOIN teams AS locals ON locals.id = matches.local
	LEFT JOIN teams AS guests ON guests.id = matches.guest
	WHERE matches.id = '$_REQUEST[match]'
	AND (locals.id = '$configuration[team]' OR guests.id = '$configuration[team]')";
$match = "";
foreach ($pdo->query($sql) as $row) {
    $match = "$row[local] - $row[guest] ";
}
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Minuti giocati dalla rosa - Scesi in campo</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        
        
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="list.php?match=<? echo $_REQUEST[match]; ?>" data-role="button" data-icon="arrow-l">Back</a>
		<h1 class="mudule-title"><img src="../../immagini/playtimes.thumb.small.png" title="." alt="." />&nbsp;&nbsp; In campo</h1>
		<div data-role="header" id="subsection-title" data-theme="e"><?echo $match;?></div>
		<div data-role="navbar">
			<ul>
				<li><a data-icon="grid" href="populate.php?match=<? echo $_REQUEST[match]; ?>" <?if(!isset($_REQUEST['giocato'])) echo 'class="ui-btn-active"';?>>Tutti</a></li>
				<li><a data-icon="alert" href="populate.php?giocato=0&match=<? echo $_REQUEST[match]; ?>" <?if(isset($_REQUEST['giocato']) && ($_REQUEST['giocato'] == 0)) echo 'class="ui-btn-active"';?>>Panchina</a></li> <!-- -->
				<li><a data-icon="star" href="populate.php?giocato=1&match=<? echo $_REQUEST[match]; ?>" <?if(isset($_REQUEST['giocato']) && ($_REQUEST['giocato'] == 1)) echo 'class="ui-btn-active"';?>>In campo</a></li>
			</ul>
		</div><!-- /navbar -->
	</div><!-- /header -->


		
	<div data-role="content">
<?php

$data = array();
$sql = "SELECT players.id as player, players.name, players.surname, players.alias, playtimes.minutes
	FROM players
	LEFT JOIN cashdesks ON players.id = cashdesks.player
		AND cashdesks.league = '$configuration[league]'
	LEFT JOIN playtimes
		ON playtimes.player = players.id
		AND playtimes.match = '$_REQUEST[match]'
  WHERE players.team = '$configuration[team]'
		AND cashdesks.player IS NOT NULL
		AND cashdesks.physical_examination = 1";

// filtro dopo aver fatto l'insert
$giocato_url = '';
	
// In campo	
if(isset($_REQUEST['giocato']) && ($_REQUEST['giocato'] == 0)) {
	$sql .= "
	AND (playtimes.minutes = 0 OR playtimes.minutes IS NULL)";
	
	$giocato_url = "&giocato=0";
}
else if(isset($_REQUEST['giocato']) && ($_REQUEST['giocato'] == 1)) {
	$sql .= "
	AND playtimes.minutes > 0";
	
	$giocato_url = "&giocato=1";
}

$sql .= "
	ORDER BY players.surname";
	
foreach ($pdo->query($sql) as $row) {
    $letter = strtoupper(substr($row['surname'], 0, 1));
    $data[$letter][] = $row;
}
?>
	
		<div class="content-primary">	
			<ul data-role="listview" data-filter="true" data-filter-placeholder="Cerca giocatori..." data-filter-theme="b">
<?php
if(sizeof($data) == 0) {

	echo '
		<li data-role="list-divider">Nessun giocatore è sceso in campo</li>';

}
?>			
				<!--<li><a href="index.html">Adam Kinkaid</a></li>-->
				<!--<li><a href="index.html">Alex Wickerham</a></li>-->
				<!--<li><a href="index.html">Avery Johnson</a></li>-->
				<!--<li data-role="list-divider">B</li>-->
<?php
foreach($data as $letter => $players) {
	echo '
				<li data-role="list-divider">'.$letter.'</li>';

	foreach($players as $player) {
	
		//  player description
		$description = $player['alias'];
		if(empty($description) || is_null($description)) {
			$description = $player['name'];
		}
		
		// Choose image profile
		$profile_image = "../../immagini/people/".strtolower($player['name'])."-".strtolower($player['surname'])."/medium.png";
		if(!file_exists($profile_image)) {
			$profile_image = "../../immagini/people/unknown/medium.png";
		}	
	
		if(is_null($player['minutes']) || $player['minutes'] == 0) {
			echo '
			<li data-role="fieldcontain">
				<a  href="update.php?player='.$player['player'].'&match='.$_REQUEST[match].''.$giocato_url.'" data-ajax="false">
					<img src="'.$profile_image.'" alt="'.$description.'" />
					<h3>'.$description.'</h3>
					<p>'.$player['name'].' '.$player['surname'].' scende in campo</p>
				</a>
				<a href="update.php?player='.$player['player'].'&match='.$_REQUEST[match].''.$giocato_url.'" data-icon="alert" data-ajax="false" data-transition="slideup">In campo</a>
			</li>';
		}
		else {
			echo '
			<li data-role="fieldcontain">
				<a  href="update.php?player='.$player['player'].'&match='.$_REQUEST[match].''.$giocato_url.'" data-ajax="false">
					<img src="'.$profile_image.'" alt="'.$description.'" />
					<h3>'.$description.'</h3>
					<p>'.$player['name'].' '.$player['surname'].' scende in campo</p>
					<span class="ui-li-count">'.$player['minutes'].'</span>
				</a>
				<a href="update.php?player='.$player['player'].'&match='.$_REQUEST[match].''.$giocato_url.'" data-icon="star" data-ajax="false" data-transition="slideup">In campo</a>
			</li>';
		}

	}				
}
?>
			</ul>
		</div><!--/content-primary -->	
                                    
	</div><!-- /content -->

	<!-- /footer <div data-position="fixed" data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div>-->
</div><!-- /page -->

</body>
</html>