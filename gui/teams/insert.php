<?php
require_once("../../libs/http.php");
require_once("../../libs/commons.php");
require_once("../../configs/config.php");
require_once("../../libs/connection.php");

// Authemntication required to access
if(!check_auth($auth_type, $realm, $users)) {
	// Redirect to login page
	unautorized();
}
?>


<!DOCTYPE html> 
<html> 
	<head> 
	<title>Feriole 1992 - Nuova squadra</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta charset="UTF-8">

	<link rel="stylesheet" href="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.css" />
        <link rel="stylesheet" href="../../stylesheets/main.css" />
	<script type="text/javascript" src="../../javascript/jquery.min.js"></script>
	<script type="text/javascript" src="../../javascript/commons.js"></script>
	<script type="text/javascript" src="../../javascript/jquery.mobile/jquery.mobile-1.0b2.min.js"></script>
        <script type="text/javascript">
        //<![CDATA[
	$(document).ready(function() {
		$("#insert").click(function() {
		    
			// Controlli
			if(isEmpty($("#name").val())) {
			    dialog("Il nome della squadra è obbligatorio");
			    return false;
			}
		    
		    
			// Sel l'autenticazione avviene correttamente carica la pagina del menu iniziale
			$.ajax({
			    type: "POST",
					cache: false,
			    data: 'insert=insert&'+$("#transaction").serialize(),
			    url: $("#transaction").attr("action"),
			    success: function() {
				$.mobile.changePage("list.php");
			    },
			    error: function(request, text, http_error_msg) {
				if(request.status != null) {
				    if(request.status == 500) {
					dialog("Si è verificato un'errore nel sistema contatta l'amministratore");
				    }
				    else if(request.status == 400) {
					dialog("Hai commesso un'errore nella compilazione della maschera");
				    }
				    else if(request.status == 409) {
					dialog(http_error_msg);
				    }
				    else {
					dialog("Error Code: "+request.status+" - "+text+" HTTP("+http_error_msg+")");
				    }
				}
				
			    }
			});
		    
			return false;
		    
		});
	});
        //]]>
        </script>
</head> 
<body> 

<div data-role="page">

	<div data-role="header">
                <a href="list.php" data-role="button" data-icon="arrow-l">Back</a>
		<h1 class="mudule-title"><img src="../../immagini/teams.thumb.small.png" title="." alt="." /> Nuova squadra</h1>
		
	</div><!-- /header -->


		
	<div data-role="content">
	
                <form data-ajax="false" class="transaction" method="POST" action="transactions.php" name="transaction" id="transaction">
                
                    <div data-role="fieldcontain">
                        <label for="name">Squadra:</label>
                        <input type="text" name="name" id="name" maxlength="25" value="" />
                    </div>
										
										<div data-role="fieldcontain">
											<label for="division" class="select">Serie:</label>
											<select name="division" id="division" data-theme="a" data-icon="gear" data-inline="true" data-native-menu="false">
												<option value="Serie A" selected="selected">A</option>
												<option value="Serie B">B</option>
											</select>
										</div>
                
                    <div class="actions" data-role="fieldcontain">
                        <input data-icon="plus" data-theme="b" type="submit" name="insert" id="insert" value="Salva" />
                    </div>
                
                </form>
                                    
	</div><!-- /content -->

	<div data-position="fixed"  data-role="footer">
		<h4>www.feriole1992.it</h4>
	</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>