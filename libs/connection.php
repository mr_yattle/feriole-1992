<?php
try {
    if(!empty($db_password) && !empty($db_user))
        $pdo = new PDO($dsn, $db_user, $db_password);
    else
        $pdo = new PDO($dsn);
}
catch(PDOException $e) {
        header("HTTP/1.0 500 Internal Server Error");
        echo $e;
        exit;
}
?>