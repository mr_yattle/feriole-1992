<?php
/**
Check auth
*/
function check_auth($auth_type, $realm, $users) {
    
    // Also start session (to store and retrieve settings also for http auth)
    session_start();
    
    if(!array_key_exists('role',$_SESSION)) {
        return false;
    }
    
    return true;
    
}

/**
Perform unauthorized operations
*/
function unautorized($page="../../index.html") {
    
    header("Location: $page");
    //die("Authorization required");
    exit;

}

/**
Perform db_normalization
*/
function dbnormalize($value=null) {
        
    if(!is_null($value)) {
        $value = htmlspecialchars($value);
    }
    return $value;

}

/**
Check if application it's usable
*/
function can_be_used($pdo) {
    
    return $pdo->query("SELECT count(*) FROM configurations")->fetchColumn();

}
?>