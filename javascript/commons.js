// Window
var $window = $( window );

// Shared logic for clearing timeout and removing message.
function hideMsg(){
        
        // Stop message show timer
        clearTimeout( 50 );
        
        // Hide loading message
        $.mobile.hidePageLoadingMsg();
};

// load message
function dialog(msg) {
    // Remove loading message.
    hideMsg();

    //show error message
    $( "<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>"+msg+"</h1></div>" )
            .css({ "display": "block", "opacity": 0.96, "top": $window.scrollTop() + 100 })
            .appendTo( $.mobile.pageContainer )
            .delay( 1000 )
            .fadeOut( 400, function() {
                    $( this ).remove();
    });
}

// detect empty
function isEmpty(obj) {
    if (typeof obj == 'undefined' || obj === null || obj === '') return true;
    if (typeof obj == 'number' && isNaN(obj)) return true;
    if (obj instanceof Date && isNaN(Number(obj))) return true;
    return false;
}